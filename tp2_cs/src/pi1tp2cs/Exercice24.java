package pi1tp2cs;

import es.Ecriture;
import es.Lecture;


public class Exercice24 {

	public static void main(String[] args) {

		// écrire votre code ici...
		
		Ecriture.uneChaine("Entrer un entier : ") ;
		int a = Lecture.unEntier();
		Ecriture.nouvelleLigne();
		
		int sum = 0;
		
		for(int i = 0; i <= a; i++){
			Ecriture.uneChaine(""+i+" ");
			sum = sum + i;
		}
		
		Ecriture.nouvelleLigne();
		Ecriture.nouvelleLigne();
		Ecriture.unEntier(sum);
		
		//pyramide
		
		Ecriture.nouvelleLigne();
		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("Entrer un entier : ") ;
		int b = Lecture.unEntier();
		Ecriture.nouvelleLigne();
		
		
		for(int i = 0; i <= b; i++){
			for(int j = 0; j < i; j++){
				Ecriture.uneChaine("*");
			}
			Ecriture.nouvelleLigne();
		}
		
		
	}

}

