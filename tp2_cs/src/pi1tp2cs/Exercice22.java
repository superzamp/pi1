package pi1tp2cs;

import es.*;
import Robot.*;

public class Exercice22 {

	public static void initializeRobotState1() {		
		Robot.initialize(3,3);
		Robot.addBlock(0,3,"?");
		Robot.addBlock(0,2,"?");
		Robot.addBlock(1,3,"?");
		Robot.addBlock(2,1,"?");
		Robot.addBlock(2,1,"?");
		Robot.addBlock(2,1,"?");
	}

	public static void initializeRobotState2() {		
		Robot.initialize(3,3);
		Robot.addBlock(0,3,"?");
		Robot.addBlock(0,2,"?");
		Robot.addBlock(1,4,"?");
		Robot.addBlock(2,3,"?");
		Robot.addBlock(2,1,"?");
		Robot.addBlock(2,3,"?");
	}
	
	public static void initializeRobotState3() {		
		Robot.initialize(3,3);
		Robot.addBlock(0,2,"?");
		Robot.addBlock(0,3,"?");
		Robot.addBlock(1,4,"?");
		Robot.addBlock(2,3,"?");
		Robot.addBlock(2,1,"?");
		Robot.addBlock(2,3,"?");
	}

	public static void initializeRobotState4() {		
		Robot.initialize(3,3);
		Robot.addBlock(0,4,"?");
		Robot.addBlock(0,6,"?");
		Robot.addBlock(1,1,"?");
		Robot.addBlock(2,7,"?");
		Robot.addBlock(2,3,"?");
		Robot.addBlock(2,2,"?");
	}
	
	public static void initializeRobotState5() {		
		Robot.initialize(3,3);
		Robot.addBlock(0,4,"?");
		Robot.addBlock(0,6,"?");
		Robot.addBlock(1,1,"?");
		Robot.addBlock(2,7,"?");
		Robot.addBlock(2,3,"?");
		Robot.addBlock(2,9,"?");
	}

	
		
	public static void main(String[] args) {
		
		// choisissez l'état initial du robot en décommentant une ligne (et une seule)
		initializeRobotState5();
		//initializeRobotState2();
		//initializeRobotState3();
		//initializeRobotState4();
		//initializeRobotState5();
	
		  // placer ici vos instructions à exécuter
		
		//condition 1
    	Robot.pickBlock();
    	int a = Robot.readBlock();
    	Robot.putBlock();
    	Robot.moveRight();
    	Robot.pickBlock();
    	int b = Robot.readBlock();
    	Robot.putBlock();
    	if( a == b ){
    		Ecriture.uneChaine("Condition n°1 vérifiée");
    		Ecriture.nouvelleLigne();
    	}
    	
    	//condition 2
    	Robot.moveRight();
    	Robot.pickBlock();
    	int c = Robot.readBlock();
    	Robot.moveLeft();
    	Robot.putBlock();
    	Robot.moveRight();
    	Robot.pickBlock();
    	int d = Robot.readBlock();
    	Robot.moveLeft();
    	Robot.putBlock();
    	Robot.moveRight();
    	Robot.pickBlock();
    	int e = Robot.readBlock();
    	Robot.putBlock();
    	if( (c!=e) && (e!=d) && (d!=c) ){
    		Ecriture.uneChaine("Condition n°2 vérifiée");
    		Ecriture.nouvelleLigne();
    	}
    	
    	//condition 3
    	Robot.moveLeft();
    	Robot.moveLeft();
    	Robot.pickBlock();
    	Robot.moveRight();
    	Robot.putBlock();
    	Robot.moveLeft();
    	Robot.pickBlock();
    	if(a > Robot.readBlock()){
    		Ecriture.uneChaine("Condition n°3 vérifiée");
    		Ecriture.nouvelleLigne();
    	}
    	Robot.putBlock();
    	
    	//condition 4
    	if( ( c+d+e) > 18 ){
    		Ecriture.uneChaine("Condition n°4 vérifiée");
    		Ecriture.nouvelleLigne();
    	}
    	
    	
    	
    	// pour démarrer le robot
    	Robot.play();		
		
	}

}

