package pi1tp2cs;

import es.*;

public class Exercice21 {

	public static void main(String[] args) {

		// écrire votre code ici...
		Ecriture.uneChaine("Entrer un entier : ") ;
		int a = Lecture.unEntier();
		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("Entrer un deuxième entier : ");
		int b = Lecture.unEntier();
		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("Le produit de ces deux entiers est "+a*b);
				
	}

}

