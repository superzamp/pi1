package pi1tp2cs;

import es.Ecriture;
import es.Lecture;

public class Exercice23 {

	public static void main(String[] args) {

		// �crire votre code ici...
		
		
		//Partie 1
		
		//entr�es utilisateur
		Ecriture.uneChaine("Entrer un entier : ") ;
		int a = Lecture.unEntier();
		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("Entrer un deuxi�me entier : ");
		int b = Lecture.unEntier();
		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("Entrer un troisi�me entier : ");
		int c = Lecture.unEntier();
		Ecriture.nouvelleLigne();
		//tri
		if(a < b){
			a = b;
		}
		if(a < c){
			a = c;
		}
		
		Ecriture.uneChaine("Le plus grand de ces trois entiers est "+a);
		
		Ecriture.nouvelleLigne();
		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("La variable est \"a\" ");
		Ecriture.nouvelleLigne();
		Ecriture.nouvelleLigne();
		
		//Partie 2
		
		//entr�es utilisateur
		Ecriture.uneChaine("Entrer un entier : ") ;
		int d = Lecture.unEntier();
		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("Entrer un deuxi�me entier : ");
		int e = Lecture.unEntier();
		Ecriture.nouvelleLigne();
		
		if( Math.abs(d) != Math.abs(e)){
			Ecriture.uneChaine("valeur absolues diff�rentes");
		} else {
			Ecriture.uneChaine("m�me valeur absolues");
		}
		
	}

}
