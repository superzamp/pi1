package pi1tp5cs;

import es.Ecriture;
import es.LectureFichierTexte;

public class Note {
	
	/* ** lireTab **
	 * lit un fichier contenant des nombres :
	 * 		- le premier nombre n est le nombre d'�l�ments � lire
	 * 		- le tableau renvoy� contient les n nombres suivants     */
    static int[] lireTab (String monfichier) {
    	/* 
    	 * en entr�e : nom du fichier � lire
    	 * en sortie : retourne un tableau d'entiers contenant les valeurs lues dans le fichier
    	 */
    	LectureFichierTexte fic = new LectureFichierTexte(monfichier); // ouverture du fichier
    	int n = fic.lireUnEntier(); // le premier entier dans le fichier est la taille du tableau
    	int[] tab = new int[n];	 // initialisation du tableau
    	for (int i=0; i<n; i=i+1) { // les indices du tableau vont de 0 � n-1
    		tab[i] = fic.lireUnEntier(); // remplissage du tableau
    	}
    	return tab;
    } // lireTab
    
    static void afficheTab (int[] tab){
    	
    	for( int i = 0; i < tab.length ; i++){
    		
    		Ecriture.uneChaine(tab[i]+"\t");
    		
    		if(i%10 == 9) Ecriture.nouvelleLigne();
    		
    	}
    	
    }
    
    static double moyenneTab(int[] tab){
    	
    	double moyenne = 0;
    	
    	for(int i = 0; i <tab.length; i++){
    		moyenne += tab[i];
    	}
    	
    	moyenne /= tab.length;
    	
    	return moyenne;
    	
    }
    
    static int[] calculHistogramme(int[] tVal){
    	
    	int[] histogramme;
    	histogramme = new int[21];
    	
    	//initialisation � 0 de l'histogramme
    	for(int i = 0; i < 21; i++){
    		histogramme[i] = 0;
    	}
    	
    	for(int i = 0; i < tVal.length; i++){
    		
    		histogramme[ tVal[i] ] += 1;
    		
    	}
    	
    	
    	return histogramme;
    	
    }
    
    static void afficheLigneHisto(int note, int nb_occurrences){
    	
    	Ecriture.uneChaine(note+"\t:\t"+nb_occurrences+"\t");
    	for(int i = 0; i < nb_occurrences; i++){
    		Ecriture.uneChaine("*");
    	}
    	
    }
    
   static void afficheHisto(int[] tab0cc){
	   
	   for(int i = 0; i < 21; i++){
		   
		   afficheLigneHisto(i, tab0cc[i]);
		   Ecriture.nouvelleLigne();
		   
	   }
	   
   }

	/* ** main **
	 * contient tous les tests    */
    public static void main(String[] arg) {
    	
		int [] tab = lireTab("/share/l1l2/pi1/tp/donnees-tp5/TP5-note.txt");
    	
    	afficheTab(tab);
    	
    	Ecriture.nouvelleLigne();
    	Ecriture.nouvelleLigne();
    	Ecriture.uneChaine("Moyenne tableau : "+moyenneTab(tab));
    	
    	Ecriture.nouvelleLigne();
    	Ecriture.nouvelleLigne();
    	afficheHisto(calculHistogramme(tab));
    	
    } // main
    
}
