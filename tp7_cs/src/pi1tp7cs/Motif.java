package pi1tp7cs;

public class Motif {
	
	public static boolean patternSearch(String motif, String chaine, int rang){
		
		//longueurs incompatibles
		if(motif.length() > chaine.length()-rang) return false;
		
		for(int i = rang; i < motif.length(); i++ ){
			
			if( chaine.charAt(i) != motif.charAt(i) && motif.charAt(i) != '*'){
				return false;
			}
			
		}
				
		
		return true;
		
	}

	public static void main(String[] args){
		
		String chaine = "tast";
		String motif = "thst";
		
		System.out.println(patternSearch(motif, chaine, 0));
		
	}
	
}
