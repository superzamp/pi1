package pi1tp7cs;

public class ADN {
	
	public static int indiceTriplet(String adn, String motif){
		
		//longueurs incompatibles
		if(adn.length() < 3) return -1;
		
		for(int i = 0; i < adn.length()-3; i++){
			
			if( adn.charAt(i) == motif.charAt(0) || motif.charAt(0) == '*'){
				if( adn.charAt(i+1) == motif.charAt(1) || motif.charAt(1) == '*'){
					if( adn.charAt(i+2) == motif.charAt(2) || motif.charAt(2) == '*'){
						return i;
					}
				}
			}
			
		}
		
		return -1;
	}
	
	public static String brinADNComplementaire(int a, int b, String adn){
		
		//longueurs incompatibles
		if( a > b || b > adn.length() )return "";
		
		String adnco = "";
		
		for(int i = a; i < b; i++){
			
			if( adn.charAt(i)  == 'A' ) adnco+="T";
			if( adn.charAt(i)  == 'T' ) adnco+="A";
			if( adn.charAt(i)  == 'G' ) adnco+="C";
			if( adn.charAt(i)  == 'C' ) adnco+="G";
		}
		
		return adnco;
	}
	
	public static String complementaireBrin(String adn, String codon1, String codon2){
		
		String adnco = "";
		
		int indice1 = indiceTriplet(adn, codon1);
		if(indice1 == -1) return "";
		
		int indice2 = indiceTriplet(adn, codon2);
		if(indice2 == -1) return "";
		
		adnco = brinADNComplementaire(indice1, indice2, adn);
		
		return adnco;
	}

	public static void main(String[] args){
		
		String adn = "ATGCGCTATAGCGCTA";
		String motif = "TAT";
		
		String codon1 = "AT*";
		String codon2 = "CTA";
		
		System.out.println(indiceTriplet(adn, motif));
		System.out.println(brinADNComplementaire(0, 5, adn));
		System.out.println(complementaireBrin(adn, codon1, codon2));
		
		
	}
	
}
