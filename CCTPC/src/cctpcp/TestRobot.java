package cctpcp;

import Robot.Robot;
import es.*;

public class TestRobot {

	
	public static void test1() {
		Robot.initialize(3,4);
		Robot.addBlock(0,3);		
		Robot.addBlock(0,6);
		Robot.addBlock(0,4);
		Robot.addBlock(0,1);
		
	}

	public static void test2() {
		Robot.initialize(3,4);
		Robot.addBlock(0,1);		
		Robot.addBlock(0,2);
	}

	public static void test3() {
		Robot.initialize(3,4);
		Robot.addBlock(0,5);
		Robot.addBlock(0,6);
		Robot.addBlock(0,7);
	}
	
	public static boolean estAutorise(int[] tab, int v){
		
		for(int i = 0; i < tab.length; i++){
			if(tab[i] == v) return true;
		}
		
		return false;
		
	}
	
	public static boolean sontAutorises(int[] tab){
		
		while( !Robot.isEmptyColumn()){
			Robot.pickBlock();
			if(!estAutorise(tab, Robot.readBlock()) ) return false;
			Robot.moveRight();
			Robot.putBlock();
			Robot.moveLeft();
		}
		
		return true;
		
	}

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// test1: num�ros tous autorises
		//int [] autorises1 = {1, 4, 6, 3};
		//test1();
		//if(sontAutorises(autorises1))Ecriture.uneChaine("ok 1");
			
		
		//test2: au moins un n'est pas autoris�
		int [] autorises2 = {4, 2, 1};
		test2();
		if(sontAutorises(autorises2))Ecriture.uneChaine("ok 2");

		// test3: au moins un n'est pas autoris�
		//int [] autorises3 = {2, 1};
		// test3();
		//if(sontAutorises(autorises3))Ecriture.uneChaine("ok 3");

		Robot.play();

	}

}
