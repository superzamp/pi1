package cctpcp;

import es.Ecriture;

public class Decaler {

	public static int[] decalerCircG(int[] tab){
		
		
		//On cr�e un nouveau tableau td
		int[] td;
		td = new int[tab.length];
		
		//On parcours tout le tableau td SAUF la derni�re case qu'on g�re manuellement
		for(int i = 0; i < tab.length -1; i++){
			td[i] = tab[i+1];
		}
		
		//On "rempli" la derni�re case de td avec la premi�re case de tab 
		td[tab.length-1] = tab[0];
		
		return td;
		
	}
	
	public static void main(String[] args){
		
		int[] tab;
		tab = new int[5];
		
		tab[0] = 3;
		tab[1] = 8;
		tab[2] = 6;
		tab[3] = 9;
		tab[4] = 10;
		
		tab = decalerCircG(tab);
		
		for(int i = 0; i < tab.length ; i++){
			Ecriture.unEntier(tab[i]);
		}
	}
	
}
