package pi1tp3cs;

import es.Ecriture;

public class Exercice_31 {
	
	public static void main(String[] args){
		
		for(int i = 0; i < 20; i++){
			Ecriture.uneChaine("i : "+(i*i));
			Ecriture.nouvelleLigne();
		}
		
		for(int i = 0; i < 100; i++){
			if( ( Math.pow(i, 3)-18*Math.pow(i, 2)+5*i-1 ) >= 0 ){
				Ecriture.uneChaine("n : "+i);
				Ecriture.nouvelleLigne();
			}
		}
		
		
		
		
	}
	
}
