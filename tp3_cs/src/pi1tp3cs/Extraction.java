package pi1tp3cs;

import Robot.*;

public class Extraction {

	public static void initExtraction1() {
		Robot.initialize(3,4);
		Robot.addBlock(0,5);		
		Robot.addBlock(0,3);
		Robot.addBlock(0,1);
	}

	public static void initExtraction2() {
		Robot.initialize(3,4);
	}

	public static void initExtraction3() {
		Robot.initialize(3,4);
		Robot.addBlock(0,5);		
	}
	
	public static void initExtraction4() {
		Robot.initialize(3,4);
		Robot.addBlock(0,3);		
	}
	

	public static void initExtraction5() {
		Robot.initialize(3,4);
		Robot.addBlock(0,4);	
		Robot.addBlock(0,3);		
		Robot.addBlock(0,2);
		Robot.addBlock(0,1);
		
	}

	// fonction extraction: prend deux entiers min et max en param�tre
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// d�commentez une ligne � la fois pour tester diff�rents �tats initiaux
		initExtraction1();
		//initExtraction2();
		//initExtraction3();
		//initExtraction4();
		//initExtraction5();
		
		// appel � la fonction d'extraction, � vous de l'�crire
		//extraction(2,4);
		
		Robot.play();
		
	}

}

