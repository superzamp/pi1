package pi1tp3cs;

import es.Ecriture;
import es.Lecture;

public class Exercice_33 {
	

	public static void main(String[] args){
		
		Ecriture.uneChaine("x? : ");
		double x = Lecture.unReel();
		
		double precision = 0.001;
		
		int i = 0;
		
		double Ti = x; 
		
		double sum = 0;
		
		
		while(Ti > precision){
			Ti = (-1)*( Math.pow(x, 2)/ ((2*i+1)*2*i) ) + Ti;
			sum = sum + Ti;
			i++;
		}
		

		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("sin(x) ~= "+sum);
		
		
	}
	
}
