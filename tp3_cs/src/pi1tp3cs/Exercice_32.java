package pi1tp3cs;

import es.Ecriture;
import es.Lecture;

public class Exercice_32 {
	
	public static void main(String[] args){
		
		Ecriture.uneChaine("n? : ");
		int n = Lecture.unEntier();
		
		int fiba = 0;
		int fibb = 1;
		
		int fibc = 0;
		
		for(int i = 0; i < n; i++){
			fibc = fiba + fibb;
			fiba = fibb;
			fibb = fibc;
			
		}
		
		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("Fib(n) : "+fiba);
		
		
	}
	
}
