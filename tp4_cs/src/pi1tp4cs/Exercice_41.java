package pi1tp4cs;

import es.Ecriture;
import es.Lecture;

public class Exercice_41 {

	public static int sommeChiffres(int n){
		
		int sum = 0;
		
		while(n != 0){
			sum = sum + (n%10);
			n = n/10;
		}
		
		return sum;
	}
	
	public static int nbRepetition(int n){
		
		int cc = 0;
		
		while(n != 0){
			if( (n%10) == ((n/10)%10) ) cc++;
			n = n/10;
		}
		
		return cc;
		
	}
	
	
	public static void main(String[] args) {
		Ecriture.uneChaine("n? : ");
		int n = Lecture.unEntier();
		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("sommeChiffres : "+sommeChiffres(n));
		Ecriture.nouvelleLigne();
		Ecriture.uneChaine("nbRepetition : "+nbRepetition(n));
	}

}
