package pi1tp4cs;

import es.Ecriture;
import es.Lecture;

public class Exercice_42 {

	public static int vitesseDiv(double a, double b){
		
		int mink = -1;
		
		double x = 0;
		double y = 0;
		
		for( int k = 0; k < 30 && mink==-1 ; k++ ){
			
			double tmpx = x;
			x = Math.pow(x, 2)-Math.pow(y,2)+a;
			y = 2*tmpx*y+b;
			
			if( Math.sqrt(Math.pow(x,2)+Math.pow(y,2)) > 2 ){
				mink = k+1;
			}
			
		}
		
		return mink;
		
	}
	
	
	public static void main(String[] args) {
		
		Ecriture.uneChaine("a? ");
		double a = Lecture.unReel();
		Ecriture.uneChaine("b? ");
		double b = Lecture.unReel();
		Ecriture.nouvelleLigne();
		
		Ecriture.unEntier(vitesseDiv(a, b));
		
		
	}

}
