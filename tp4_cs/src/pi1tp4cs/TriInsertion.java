package pi1tp4cs;

import Robot.*;

public class TriInsertion {

	public static void testIntertion1() {
		Robot.initialize(3,4);
		Robot.addBlock(0,1);		
		Robot.addBlock(1,2);		
	}

	public static void testIntertion2() {
		Robot.initialize(3,4);
		Robot.addBlock(0,1);		
		Robot.addBlock(0,3);		
		Robot.addBlock(1,2);		
	}

	public static void testIntertion3() {
		Robot.initialize(3,4);
		Robot.addBlock(0,3);		
		Robot.addBlock(0,4);		
		Robot.addBlock(1,2);		
	}

	
	public static void testTri1() {
		Robot.initialize(3,4);
		Robot.addBlock(1,1,"?");		
		Robot.addBlock(1,3,"?");
		Robot.addBlock(1,2,"?");
	}

	public static void testTri2() {
		Robot.initialize(3,5);
		Robot.addBlock(1,1,"?");		
		Robot.addBlock(1,4,"?");
		Robot.addBlock(1,3,"?");
		Robot.addBlock(1,2,"?")
		;
	}

	public static void testTri3() {
		// cas le pire ...
		Robot.initialize(3,4);
		Robot.addBlock(1,4,"?");		
		Robot.addBlock(1,3,"?");
		Robot.addBlock(1,2,"?");
		Robot.addBlock(1,1,"?");
	}

	public static void testTri4() {
		// cas le pire ...
		Robot.initialize(3,6);
		Robot.addBlock(1,4,"?");		
		Robot.addBlock(1,3,"?");
		Robot.addBlock(1,5,"?");		
		Robot.addBlock(1,2,"?");
		Robot.addBlock(1,6,"?");
		Robot.addBlock(1,1,"?");
	}

	public static void testTri5() {
		Robot.initialize(3,3);
		Robot.addBlock(1,4,"?");		
	}
	
	public static void inserer(){
		
	}
	
	public static void triInsertion(){
		
	}

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// d�commentez une ligne � la fois pour tester diff�rents �tats initiaux pour l'insertion		
		//testIntertion1();
		//testIntertion2();
		//testIntertion3();
		
		// test de l'algorithme d'insertion. Dans les donn�es de tests, on cherche toujours
		// � ins�rer le bloc dont le num�ro est 2
		// inserer(2);
		
		
		// d�commentez une ligne � la fois pour tester diff�rents �tats initiaux pour le tri			
		testTri1();
		//testTri2()
		//testTri3();
		//testTri4();
		//testTri5();

		
		// test de l'algorithme de tri insertion
		triInsertion();
		
		Robot.play();
	}

}

