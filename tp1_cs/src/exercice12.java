
import Robot.*;


public class exercice12 {

	
	public static void initializeRobotState1() {
		// creation d'un environnement 3 colonnes, chacune contenant 3 blocs maximum
		Robot.initialize(3,3);
		// ajout d'un bloc avec le numero "1" dans la premi�re colonne (colonne d'indice 0)
		// le numero est remplac� par un "?"
		Robot.addBlock(0,1,"?");
		
		// ajout d'un bloc avec le numero "2" dans la premi�re colonne (colonne d'indice 0)
		Robot.addBlock(0,2,"?");
		
		// ajout d'un bloc avec le numero "2" dans la premi�re colonne (colonne d'indice 0)
		Robot.addBlock(0,3,"?");		
	}


	public static void initializeRobotState2() {		
		// creation d'un environnement 3 colonnes, chacune contenant 3 blocs maximum
		Robot.initialize(3,3);
		Robot.addBlock(0,7,"?");
		Robot.addBlock(0,4,"?");
		Robot.addBlock(0,6,"?");		
	}

	public static void initializeRobotState3() {		
		// creation d'un environnement 3 colonnes, chacune contenant 3 blocs maximum
		Robot.initialize(3,3);
		Robot.addBlock(0,3,"?");
		Robot.addBlock(0,2,"?");
		Robot.addBlock(0,1,"?");		
	}
	
	public static void initializeRobotState4() {		
		// creation d'un environnement 3 colonnes, chacune contenant 3 blocs maximum
		Robot.initialize(3,3);
		Robot.addBlock(0,1,"?");
		Robot.addBlock(0,7,"?");
		Robot.addBlock(0,3,"?");		
	}

	public static void initializeRobotState5() {
		// creation d'un environnement 3 colonnes, chacune contenant 3 blocs maximum
		Robot.initialize(3,3);
		Robot.addBlock(0,4,"?");
		Robot.addBlock(0,2,"?");
		Robot.addBlock(0,6,"?");		
	}

		
	public static void main(String[] args) {
		
		// choisissez l'�tat initial du robot en d�commentant une ligne (et une seule)
		initializeRobotState1();
		//initializeRobotState2();
		//initializeRobotState3();
		//initializeRobotState4();
		//initializeRobotState5();
		
        // placer ici vos instructions � ex�cuter
		int n = 0;
		
    	Robot.pickBlock();
    	if(Robot.readBlock() > n){
    		n = Robot.readBlock();
    	}
    	Robot.moveRight();
    	Robot.putBlock();
    	Robot.moveLeft();
    	
    	Robot.pickBlock();
    	if(Robot.readBlock() > n){
    		n = Robot.readBlock();
    	}
    	Robot.moveRight();
    	Robot.putBlock();
    	Robot.moveLeft();
    	
    	Robot.pickBlock();
    	if(Robot.readBlock() > n){
    		n = Robot.readBlock();
    	}
    	Robot.moveRight();
    	Robot.putBlock();
    	
    	Robot.pickBlock();
    	if(Robot.readBlock() == n){
    		Robot.moveRight();
        	Robot.putBlock();
        	Robot.moveLeft();
    	} else {
    		Robot.moveLeft();
    		Robot.putBlock();
    		Robot.moveRight();
    	}
    	
    	Robot.pickBlock();
    	if(Robot.readBlock() == n){
    		Robot.moveRight();
        	Robot.putBlock();
        	Robot.moveLeft();
    	} else {
    		Robot.moveLeft();
    		Robot.putBlock();
    		Robot.moveRight();
    	}
    	
    	Robot.pickBlock();
    	if(Robot.readBlock() == n){
    		Robot.moveRight();
        	Robot.putBlock();
        	Robot.moveLeft();
    	} else {
    		Robot.moveLeft();
    		Robot.putBlock();
    		Robot.moveRight();
    	}
    	
    	Robot.moveLeft();
    	n = 0;
    	
    	Robot.pickBlock();
    	if(Robot.readBlock() > n){
    		n = Robot.readBlock();
    	}
    	Robot.moveRight();
    	Robot.putBlock();
    	Robot.moveLeft();
    	
    	Robot.pickBlock();
    	if(Robot.readBlock() > n){
    		n = Robot.readBlock();
    	}
    	Robot.moveRight();
    	Robot.putBlock();
    	
    	Robot.pickBlock();
    	if(Robot.readBlock() == n){
    		Robot.moveRight();
        	Robot.putBlock();
        	Robot.moveLeft();
    	} else {
    		Robot.moveLeft();
    		Robot.putBlock();
    		Robot.moveRight();
    	}
    	
    	Robot.pickBlock();
    	if(Robot.readBlock() == n){
    		Robot.moveRight();
        	Robot.putBlock();
        	Robot.moveLeft();
    	} else {
    		Robot.moveLeft();
    		Robot.putBlock();
    		Robot.moveRight();
    	}
    	
    	Robot.moveLeft();
    	Robot.pickBlock();
    	Robot.moveRight();
    	Robot.moveRight();
    	Robot.putBlock();
    	  	
    	
    	
    	
    	// pour d�marrer le robot
    	Robot.play();
		
	}

}
