
import Robot.*;


public class exercice11 {

	
	public static void initializeRobotState1() {
		
		// creation d'un environnement 3 colonnes, chacune contenant 3 blocs maximum
		Robot.initialize(3,4);
		
		// ajout d'un bloc avec le numero "1" dans la premi�re colonne (colonne d'indice 0)
		// le numero est remplac� par un "?"
		Robot.addBlock(0,1,"?");
		
		// ajout d'un bloc avec le numero "2" dans la premi�re colonne (colonne d'indice 0)		
		Robot.addBlock(0,2,"?");
		
		// ajout d'un bloc avec le numero "1" dans la premi�re colonne (colonne d'indice 0)
		Robot.addBlock(0,1,"?");		
	}


	public static void initializeRobotState2() {		
		// creation d'un environnement 3 colonnes, chacune contenant 3 blocs maximum
		Robot.initialize(3,3);
		Robot.addBlock(0,1,"?");
		Robot.addBlock(0,1,"?");
		Robot.addBlock(0,1,"?");		
	}

	public static void initializeRobotState3() {		
		// creation d'un environnement 3 colonnes, chacune contenant 3 blocs maximum
		Robot.initialize(3,3);
		Robot.addBlock(0,2,"?");
		Robot.addBlock(0,2,"?");
		Robot.addBlock(0,1,"?");		
	}
	
	public static void initializeRobotState4() {		
		// creation d'un environnement 3 colonnes, chacune contenant 3 blocs maximum
		Robot.initialize(3,3);
		Robot.addBlock(0,2,"?");
		Robot.addBlock(0,2,"?");
		Robot.addBlock(0,2,"?");		
	}
		
	public static void main(String[] args) {
		
		// choisissez l'�tat initial du robot en d�commentant une ligne (et une seule)
		initializeRobotState1();
		//initializeRobotState2();
		//initializeRobotState3();
		//initializeRobotState4();
				
        // placer ici vos instructions � ex�cuter
		for(int i=0; i < 3; i++){
			Robot.pickBlock();
	    	Robot.moveRight();
	    	if(Robot.readBlock() == 1){
	    		Robot.putBlock();
	    	} else {
	    		Robot.moveRight();
	    		Robot.putBlock();
	    		if(i != 2){
	    			Robot.moveLeft();
	    		}
	    	}
	    	if(i != 2){
    			Robot.moveLeft();
    		}
		}
    	    	
    	
    	// pour d�marrer le robot
    	Robot.play();
		
	}

}
